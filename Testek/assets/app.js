/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */



// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

import $ from 'jquery';

$(document).ready(function() {
    var $choicewrapper = $('.js-choice-wrapper');
    $choicewrapper.on('click', '.js-remove-choice', function(e) {
        e.preventDefault();

        $(this).closest('.js-choice-item')
            .fadeOut()
            .remove();
    });

    $choicewrapper.on('click', '.js-choice-add', function(e) {
        e.preventDefault();
        var prototype = $choicewrapper.data('prototype');
        var index = $choicewrapper.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        $choicewrapper.data('index', index + 1);
        $(this).before(newForm);
    });


    var $answerwrapper = $('.js-answer-wrapper');

    $answerwrapper.each(function () {
        var wrapper = $(this);
        $(this).on('click', '.js-remove-answer', function(e) {
            e.preventDefault();

            $(this).closest('.js-answer-item')
                .fadeOut()
                .remove();
        });
        $(this).on('click', '.js-answer-add', function(e) {
            e.preventDefault();
            var prototype = wrapper.data('prototype');
            var index = wrapper.data('index');
            var newForm = prototype.replace(/__name__/g, index);
            $answerwrapper.data('index', index + 1);
            $(this).before(newForm);
        });

    });


    var $textwrapper = $('.js-text-wrapper');
    $textwrapper.on('click', '.js-text-remove', function(e) {
        e.preventDefault();

        $(this).closest('.js-text-item')
            .fadeOut()
            .remove();
    });
    $textwrapper.on('click', '.js-text-add', function(e) {
        e.preventDefault();
        var prototype = $textwrapper.data('prototype');
        var index = $textwrapper.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        newForm = newForm.replace(/%index%/g, index + 1);
        $textwrapper.data('index', index + 1);
        $(this).before(newForm);
    });
});

