<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417112353 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student_group ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student_group ADD CONSTRAINT FK_E5F73D58727ACA70 FOREIGN KEY (parent_id) REFERENCES student_group (id)');
        $this->addSql('CREATE INDEX IDX_E5F73D58727ACA70 ON student_group (parent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student_group DROP FOREIGN KEY FK_E5F73D58727ACA70');
        $this->addSql('DROP INDEX IDX_E5F73D58727ACA70 ON student_group');
        $this->addSql('ALTER TABLE student_group DROP parent_id');
    }
}
