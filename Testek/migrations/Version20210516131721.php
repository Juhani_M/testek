<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210516131721 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE taken_tests (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, date DATE NOT NULL, questions LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', awnsers LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', grade VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_AF77C8ACCB944F1A (student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE taken_tests ADD CONSTRAINT FK_AF77C8ACCB944F1A FOREIGN KEY (student_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE taken_tests');
    }
}
