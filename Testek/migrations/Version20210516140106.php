<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210516140106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taken_tests ADD test_id INT NOT NULL');
        $this->addSql('ALTER TABLE taken_tests ADD CONSTRAINT FK_AF77C8AC1E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('CREATE INDEX IDX_AF77C8AC1E5D0459 ON taken_tests (test_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taken_tests DROP FOREIGN KEY FK_AF77C8AC1E5D0459');
        $this->addSql('DROP INDEX IDX_AF77C8AC1E5D0459 ON taken_tests');
        $this->addSql('ALTER TABLE taken_tests DROP test_id');
    }
}
