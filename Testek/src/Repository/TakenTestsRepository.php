<?php

namespace App\Repository;

use App\Entity\TakenTests;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TakenTests|null find($id, $lockMode = null, $lockVersion = null)
 * @method TakenTests|null findOneBy(array $criteria, array $orderBy = null)
 * @method TakenTests[]    findAll()
 * @method TakenTests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TakenTestsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TakenTests::class);
    }

    // /**
    //  * @return TakenTests[] Returns an array of TakenTests objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TakenTests
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
