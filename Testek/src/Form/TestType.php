<?php

namespace App\Form;

use App\Entity\StudentGroup;
use App\Entity\Test;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class TestType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Testi nimi',
            ])
            ->add('multipleChoice', CollectionType::class, [
                'entry_type' => MultipleChoiceType::class,
                'label' => 'Valikuga küsimused',
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
            ])
            ->add('textQuestion', CollectionType::class, [
                'entry_type' => TextType::class,
                'label' => 'Tavalised küsimused',
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'block_name' => 'text_questions',
                'entry_options' => [
                    'label' => 'Küsimus %index%',
                ]

            ])
            ->add('timeToComplete', IntegerType::class,[
                'label' => 'Aeg testi tegemiseks',
                'help' => 'jata tühjaks, et aeg oleks piiramatu',
                'required' => false,
            ])
            ->add('expirationDate', DateType::class,[
                'label' => 'Viimane lubatud vastamise kuupäev',
                'required' => false,
                'widget' => 'single_text'
            ])
        ;
            if (!$this->security->getUser()->isStudent()) {
                $builder->add('assignedTo',EntityType::class, [
                    'class' => StudentGroup::class,
                    'label' => 'gruppidele',
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => false,
                ])
                ;
            }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Test::class,
        ]);
    }
}