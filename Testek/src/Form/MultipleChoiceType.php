<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class MultipleChoiceType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextareaType::class,[
                'label' => 'Küsimus',
            ])
            ->add('answers', CollectionType::class,[
                'entry_type' => QuestionWithCheckboxType::class,
                'label' => 'vastused',
                'help' => 'margi oige vastus linnukesega',
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ;
    }
}