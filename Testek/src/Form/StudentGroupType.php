<?php

namespace App\Form;

use App\Entity\StudentGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'grupi nimi',
                'required' => true,
            ])
            ->add('parent', EntityType::class, [
                'class' => StudentGroup::class,
                'label' => 'vanem grupp',
                'choice_label' => 'name',
                'attr' => ['readonly' => true],
                'disabled' => !$options['superAdmin'],
                'data' => $options['group'],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StudentGroup::class,
            'superAdmin' => false,
            'group' => null,
        ]);
    }
}
