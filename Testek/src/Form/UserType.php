<?php

namespace App\Form;

use App\Entity\Group;
use App\Entity\StudentGroup;
use App\Entity\User;
use App\Security\LoginAuthenticator;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportTrait;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Unique;

class UserType extends AbstractType
{
    /**
     * @var Security
     */
    private  $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['generate']) {
            $builder
                ->add('email',EmailType::class, [
                    'label' => 'email'
                ])

                ->add('password', HiddenType::class, [
                    'data' => 'generate password'
                ])
                ->add('studentGroup', EntityType::class, [
                    'class' => StudentGroup::class,
                    'choice_label' => 'name',
                    'label' => 'grupp',
                    'required' => false,
                ])
            ;
            if ($this->security->getUser()->isSuperAdmin()) {
                $builder
                    ->add('roles', ChoiceType::class,[
                        'choices' => [
                            'teacher' => 'ROLE_TEACHER',
                            'student' => 'ROLE_STUDENT',
                        ],
                        'multiple' => true,
                        'expanded' => true,
                        'label' => 'rollid'

                    ]);
            }
        } else {
            $builder
                ->add('email', EmailType::class, [
                    'label' => 'Email',
                ])
                ->add('name', TextType::class,[
                    'label' => 'Nimi',
                    'constraints' => [new Length(['max' => 128, 'maxMessage' => 'Nimi peab olema vähem kui 128 tähte'])]
                ])
                ->add('password', RepeatedType::class,[
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'Salasona'],
                    'second_options' => ['label' => 'Korda salasona'],
                    'constraints' => [
                        new Length(['min' => 8, 'minMessage' => 'Salasõna peab olema vähemalt 8 tähte']),
                    ],
                    'invalid_message' => 'Salasõnad ei ühti',
                ])
            ;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'generate' => false,
        ]);
    }
}
