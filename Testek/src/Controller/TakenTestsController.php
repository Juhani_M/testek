<?php

namespace App\Controller;

use App\Entity\TakenTests;
use App\Entity\Test;
use App\Form\StudentGroupType;
use App\Form\TakenTest;
use App\Repository\TakenTestsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TakenTestsController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * @Route("/taken/tests", name="taken_tests")
     */
    public function index(TakenTestsRepository $testRepository): Response
    {
        $user = $this->getUser();
        $takenTests = $testRepository->findBy(['student' => $user]);
        return $this->render('taken_tests/index.html.twig', [
            'tests' => $takenTests,
        ]);
    }

    /**
     * @Route("/take_test/{id}", name="take_test")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function takeTest(Request $request, int $id): Response
    {
        $test = $this->em->getRepository(Test::class)->findOneBy(['id' => $id]);
        $takenTest = new TakenTests();
        $formOptions = [];

        $form = $this->createForm(TakenTest::class, $takenTest, $formOptions);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($test);
            $em->flush();
            $this->addFlash('success', 'Salvestatud edukalt');

            return $this->redirectToRoute('list');
        }
        return $this->render('taken_tests/take.html.twig', [
            'test' => $test,
            'form' => $form->createView(),
        ]);
    }
}
