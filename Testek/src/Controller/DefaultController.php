<?php

// src/Controller/DefaultController.php
namespace App\Controller;

use App\Main\Login\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index()
    {
        $formtype = $this->createForm(LoginType::class);

        return $this->render('login.html.twig',[
            'form' => $formtype->createView(),
        ]);
    }
}