<?php

namespace App\Controller;

use App\Entity\StudentGroup;
use App\Form\StudentGroupType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class StudentGroupController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/student/group", name="student_group")
     */
    public function index(): Response
    {
        return $this->render('StudentGroup/index.html.twig', [
            'controller_name' => 'StudentGroupController',
        ]);
    }

    /**
     * @Route("/student/group/edit/{id}", name="edit_student_group")
     * @param Request $request
     * @param int|null $id
     * @return Response
     */
    public function editAction(Request $request, int $id = null)
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        if ($id) {
            $group = $this->em->find(StudentGroup::class, $id);
            if (!$group) {
                throw new NotFoundHttpException('Record does not exist');

            }
        } else {
            $group = new StudentGroup();
        }
        $formOptions = [];
        $user = $this->getUser();
        if ($user->isSuperAdmin()) {
            $formOptions['superAdmin'] = true;
        } else {
            $group = $user->getStudentGroup();
            $formOptions['group'] = $group;
            $group->setParent($group);
        }
        $form = $this->createForm(StudentGroupType::class, $group, $formOptions);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var StudentGroup $group */
            $group = $form->getData();
            $this->em->persist($group);
            $this->em->flush();
            $this->addFlash('success', 'Uus grupp loodud');

        }
        return $this->render('StudentGroup/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
