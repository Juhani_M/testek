<?php

namespace App\Controller;

use App\Entity\Test;
use App\Form\TestType;
use App\Repository\TestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use function Doctrine\ORM\QueryBuilder;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="addTest")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function save(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $test = new Test();

        $form = $this->createForm(TestType::class, $test);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($test);
            $em->flush();
            $this->addFlash('success', 'Salvestatud edukalt');

            return $this->redirectToRoute('list');
        }

        #$this->addFlash('error', 'probleem');
        return $this->render('test/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/list", name="list")
     * @param TestRepository $testRepository
     * @return Response
     */
    public function list(TestRepository $testRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();
        if (in_array('ROLE_STUDENT', $user->getRoles())) {
            // or if student made themselves.
            $qb = $testRepository->createQueryBuilder('m');
            $qb->leftJoin("m.assignedTo", 'gr');
            $qb->andWhere($qb->expr()->eq('gr.id', ':id'));
            $qb->setParameter('id', $user->getStudentGroup());
            $tests = $qb->getQuery()->getResult();
        } else {
            $tests = $testRepository->findAll();
        }


        return $this->render('test/list.html.twig',[
            'tests' => $tests,
        ]);
    }

    /**
     * @param Test $test
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     * @route("/test/{id}/edit", name="edit")
     */
    public function edit($id, Request $request, EntityManagerInterface $em) {
        $this->denyAccessUnlessGranted('ROLE_USER');
        // and owner of test ?
        $test = $em->find(Test::class,$id);
        if (!($test instanceof Test)) {
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(TestType::class, $test);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Test $article */
            $article = $form->getData();
            $em->persist($article);
            $em->flush();
            $this->addFlash('success', 'Andmed salvestatud');
            return $this->redirectToRoute('edit',[
                'id' => $test->getId(),
            ]);
        }
        return $this->render('test/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }




}