<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;


    /**
     * @var StudentGroup|null
     * @ORM\ManyToOne(targetEntity=StudentGroup::class, inversedBy="name")
     */
    private $studentGroup;

    /**
     * @ORM\OneToOne(targetEntity=TakenTests::class, mappedBy="student", cascade={"persist", "remove"})
     */
    private $takenTests;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return StudentGroup|null
     */
    public function getStudentGroup(): ?StudentGroup
    {
        return $this->studentGroup;
    }

    /**
     * @param StudentGroup|null $studentGroup
     */
    public function setStudentGroup(?StudentGroup $studentGroup): void
    {
        $this->studentGroup = $studentGroup;
    }

    public function isSuperAdmin()
    {
        return in_array('ROLE_SUPER_ADMIN', $this->roles, true);
    }
    public function isStudent()
    {
        return in_array('ROLE_STUDENT', $this->roles, true);
    }

    public function getTakenTests(): ?TakenTests
    {
        return $this->takenTests;
    }

    public function setTakenTests(TakenTests $takenTests): self
    {
        // set the owning side of the relation if necessary
        if ($takenTests->getStudent() !== $this) {
            $takenTests->setStudent($this);
        }

        $this->takenTests = $takenTests;

        return $this;
    }




}
