<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $multipleChoice = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $textQuestion = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $TimeToComplete;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $groups = [];

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $expirationDate;

    /**
     * @ORM\ManyToMany(targetEntity=StudentGroup::class, inversedBy="tests")
     */
    private $assignedTo;

    /**
     * @ORM\OneToMany(targetEntity=TakenTests::class, mappedBy="test")
     */
    private $takenTests;

    public function __construct()
    {
        $this->assignedTo = new ArrayCollection();
        $this->takenTests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMultipleChoice(): ?array
    {
        return $this->multipleChoice;
    }

    public function setMultipleChoice(?array $multipleChoice): self
    {
        $this->multipleChoice = $multipleChoice;

        return $this;
    }

    public function getTextQuestion(): ?array
    {
        return $this->textQuestion;
    }

    public function setTextQuestion(?array $textQuestion): self
    {
        $this->textQuestion = $textQuestion;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTimeToComplete(): ?int
    {
        return $this->TimeToComplete;
    }

    public function setTimeToComplete(?int $TimeToComplete): self
    {
        $this->TimeToComplete = $TimeToComplete;

        return $this;
    }

    public function getGroups(): ?array
    {
        return $this->groups;
    }

    public function setGroups(?array $groups): self
    {
        $this->groups = $groups;

        return $this;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * @return Collection|StudentGroup[]
     */
    public function getAssignedTo(): Collection
    {
        return $this->assignedTo;
    }

    public function addAssignedTo(StudentGroup $assignedTo): self
    {
        if (!$this->assignedTo->contains($assignedTo)) {
            $this->assignedTo[] = $assignedTo;
        }

        return $this;
    }

    public function removeAssignedTo(StudentGroup $assignedTo): self
    {
        $this->assignedTo->removeElement($assignedTo);

        return $this;
    }

    /**
     * @return Collection|TakenTests[]
     */
    public function getTakenTests(): Collection
    {
        return $this->takenTests;
    }

    public function addTakenTest(TakenTests $takenTest): self
    {
        if (!$this->takenTests->contains($takenTest)) {
            $this->takenTests[] = $takenTest;
            $takenTest->setTest($this);
        }

        return $this;
    }

    public function removeTakenTest(TakenTests $takenTest): self
    {
        if ($this->takenTests->removeElement($takenTest)) {
            // set the owning side to null (unless already changed)
            if ($takenTest->getTest() === $this) {
                $takenTest->setTest(null);
            }
        }

        return $this;
    }
}
